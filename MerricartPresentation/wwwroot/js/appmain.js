﻿

validateSession();
allowOnlyAccessibleModule();
var idleTime = 0;
$(document).ready(function () {

    //Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Nuturalize the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
        $("#logout-alert-dialog").modal("hide");

    });
    $(this).keypress(function (e) {
        idleTime = 0;
        $("#logout-alert-dialog").modal("hide");
    });

    $('.treeview-menu').on('click', function (e) {
        $('.treeview-menu').removeClass('active');
    })

    setTimeout(function () {
        drawNavigation();
        setPasswordEyeEvent();
    }, 200);


});


$(window).on('load', function () {
    setUserInfo();
    setUserImage();
});

function allowOnlyAccessibleModule() {

    var accessibleUrlList = JSON.parse(localStorage.getItem('accessibleUrlList'));
    accessibleUrlList.push('/');
    accessibleUrlList.push('/Modules/User/UserProfile');
    var index = accessibleUrlList.indexOf(location.pathname);
    var altIndex = accessibleUrlList.indexOf(location.pathname.substring(0, location.pathname.length - 1));
    if (index < 0 && altIndex < 0) {
        location.href = location.origin + accessibleUrlList[0];
    }
}

function setActiveMenu() {
    try {
        var navLinkList = $('.nav-link');
        for (var i = 0; i < navLinkList.length; i++) {
            try {
                var navlink = navLinkList[i];
                if (window.location.pathname.split('#')[0].toLowerCase() == navlink.attributes['nav-page'].value.toLowerCase()) {
                    $('#' + navlink.id).addClass('active');
                    if (navlink.attributes["parent"] != null) {
                        var parentMenu = $('#' + navlink.attributes["parent"].value);
                        parentMenu.attr('aria-expanded', true);
                        var parentMenuAriaControls = parentMenu.attr('aria-controls');
                        $('#' + parentMenuAriaControls).addClass('show');
                    }
                    break;
                }
            } catch (e) { }
        }
    }
    catch (e) { }
}


function timerIncrement() {
    idleTime = idleTime + 1;
    var logoutTimerValue = $.cookie("idleLogoutTime") - idleTime;
    if (logoutTimerValue <= 2) {
        $("#logout-timer").text(logoutTimerValue);
        $("#logout-alert-dialog").modal("show");
    }
    if (idleTime >= $.cookie("idleLogoutTime")) {
        logoutUser();
    }
}
var apiUrl = "http://localhost:52696/";
var appS3Url = "https://merricart-dev.s3.ap-south-1.amazonaws.com/";

function validateSession() {

    document.cookie.split(/; */).forEach(function (cookieraw) {
        try {
            var cookieValue = cookieraw.split('=')[1];
            if (cookieValue == null || cookieValue == "") {
                logoutUser();
            }
        }
        catch (e) { }
    });
}

function setUserInfo() {
    try {

        var userNameElements = $('.user-name');
        for (var i = 0; i < userNameElements.length; i++) {
            userNameElements[i].innerText = localStorage.getItem('firstName') + ' ' + localStorage.getItem('lastName');
        }

        var userRoleElements = $('.user-role');
        for (var i = 0; i < userRoleElements.length; i++) {
            userRoleElements[i].innerText = '';
            if ($.cookie('isAdmin') == 'true') {
                userRoleElements[i].innerText = 'Admin';
            }
        }

    } catch (e) {

    }
}

function setUserImage(userImage) {
    if (userImage != null && userImage != "") {
        localStorage.setItem('userImage', userImage);
    }

    var userImageUrl = appS3Url + "user/" + localStorage.getItem('userImage');
    $(".user-image").attr('src', userImageUrl)

}

function logoutUser() {
    try {
        ClearCookieAndSignOut();
    } catch (e) { }
    window.location.href = window.location.origin + '/login';

}

function ClearCookieAndSignOut() {
    $.ajax({
        type: 'GET',
        url: apiUrl + "api/authentication/SignoutUser/",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('authorizationtoken', $.cookie("sessionToken"));
        },
        headers: {
            "content-type": "application/json",
            "cache-control": "no-cache",
        },
        dataType: "json"
    });


    document.cookie.split(/; */).forEach(function (cookieraw) {
        try {
            $.removeCookie(cookieraw.split('=')[0], { path: '/' });
        }
        catch (e) { }
    });
    localStorage.removeItem('navigation');
    localStorage.removeItem('accessibleUrlList');
    localStorage.removeItem('firstName');
    localStorage.removeItem('lastName');
    localStorage.removeItem('userImage');
}

function drawNavigation() {
    var moduleList = JSON.parse(localStorage.getItem('navigation'));
    var navigationTree = "";

    $.each(moduleList, function (parentIndex, e) {
        if (e.url != null && e.url != "") {
            navigationTree += '     <li class="nav-item"> ';
            navigationTree += '         <a class="nav-link" href="' + e.url + '" nav-page="' + e.url + '" role="button" id="sidebar' + e.moduleName + 'Menu"> ';
            navigationTree += '             <i class="' + e.icon + '" data-fa-transform="left-6"></i> ' + e.moduleName + ' ';
            navigationTree += '         </a> ';
            navigationTree += '     </li> ';
        }
        else {
            navigationTree += '     <li class="nav-item"> ';
            navigationTree += '         <a class="nav-link" href="#sidebar' + e.moduleName + '" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebar' + e.moduleName + '" id="sidebar' + e.moduleName + 'Menu"> ';
            navigationTree += '             <i class="' + e.icon + '" data-fa-transform="left-6"></i> ' + e.moduleName + ' ';
            navigationTree += '         </a> ';

            if (e.children != null && e.children.length > 0) {
                navigationTree += '         <div class="collapse" id="sidebar' + e.moduleName + '"> ';

                $.each(e.children, function (childIndex, child) {
                    navigationTree += '             <ul class="nav nav-sm flex-column"> ';
                    navigationTree += '                 <li class="nav-item"> ';
                    navigationTree += '                     <a href="' + child.url + '" nav-page="' + child.url + '" class="nav-link" id="sidebar' + e.moduleName + 'Menu-' + child.moduleName + '" parent="sidebar' + e.moduleName + 'Menu"> ';
                    navigationTree += '                         ' + child.moduleName + ' ';
                    navigationTree += '                     </a> ';
                    navigationTree += '                 </li> ';
                    navigationTree += '             </ul> ';
                });

                navigationTree += '         </div> ';
            }
            navigationTree += '     </li> ';
        }
    })

    navigationTree += '     <li class="nav-item d-md-none"> ';
    navigationTree += '         <a class="nav-link" data-toggle="modal"> ';
    navigationTree += '             <span class="fe fe-bell"></span> Notifications ';
    navigationTree += '         </a> ';
    navigationTree += '     </li> ';

    $('#app-nav').html(navigationTree);

    setActiveMenu();
}

$(document).ajaxError(function myErrorHandler(event, xhr, ajaxOptions, thrownError) {
    if (xhr.status == 401 && xhr.statusText == "Unauthorized") {
        ClearCookieAndSignOut();
        $('#loggedout-indication-modal').modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#loggedout-indication-modal").show();
    }
    else {
        waitMe_hide('#app-page-content');
        toastr["error"]("Something went wrong!")
    }
});

function initializeImageCropper(imageType) {
    waitMe_show('#app-page-content');
    $("#cropper-html").load(window.location.origin + '/plugins/cropperjs/html/custom-cropper.html');

    setTimeout(function () {
        if (imageType == 'user') {
            $("#aspectRatio3").click();
            $('.btn-group.d-flex.flex-nowrap').attr("style", "display: none !important");;
            $('#cropped-preview-btn-grp').attr("style", "display: none !important");
            //$("#inputImage").click();
        }
        else if (imageType == 'category') {
            $("#aspectRatio1").click();
        }
        else if (imageType == 'product') {

        }
        uploadImageType = imageType;
        $('#image-cropper-modal').modal('show');
        waitMe_hide('#app-page-content');

    }, 1500);
}
function destroyImageCropper() {
    $('#image-cropper-modal').modal('hide');
    waitMe_hide('#app-page-content');
}

function drawProductImage(imgSrc, isDataUrl) {
    var fileValue = '';
    if (isDataUrl == true) {
        fileValue = Date.now()
        newProductImageDataUrl.push(imgSrc);
        newProductImageDataUrlTempId.push(fileValue);
    }
    else {
        fileValue = imgSrc.split('.')[0];
        imgSrc = appS3Url + "products/" + imgSrc;
    }

    appendProductImage(imgSrc, fileValue);
}

function appendProductImage(imgSrc, fileValue) {

    var imagePreviewContainer = '';
    imagePreviewContainer += ' <div id="' + fileValue + '" class="col-sm-12 col-md-6 img-product-preview" style="float:left;"><div data-toggle="tooltip" title="Remove" onclick="removeProductImage(event,\'' + fileValue + '\')" class="product-img-remove"><i class="fe fe-x-circle"></i></div>';
    imagePreviewContainer += '     <img style="width:100%;" src="' + imgSrc + '" class="img-square">';
    imagePreviewContainer += '     <div class="middle">';
    imagePreviewContainer += '         <div><i class="fe fe-camera" style="font-size:xx-large;" aria-hidden="true"></i></div>';
    imagePreviewContainer += '     </div>';
    imagePreviewContainer += ' </div>';

    $("#product-img-list-container").append(imagePreviewContainer);
}

function clearProductImage() {
    $("#product-img-list-container").html('');
    newProductImageDataUrl = [];
}

//#region show/hide password

function setPasswordEyeEvent() {

    $('.password-eye').on('mousedown', function (e) {
        showPassword(e);
    })

    $('.password-eye').on('mouseup', function (e) {
        hidePassword(e);
    })
}

function showPassword(e) {
    try {
        var controlId = e.target.parentElement.attributes['control-for'].value;
        $('#' + controlId).attr('type', 'text')
    } catch (e) {

    }
}
function hidePassword(e) {
    try {
        var controlId = e.target.parentElement.attributes['control-for'].value;
        $('#' + controlId).attr('type', 'password')
    } catch (e) {

    }
}

//#endregion show/hide password