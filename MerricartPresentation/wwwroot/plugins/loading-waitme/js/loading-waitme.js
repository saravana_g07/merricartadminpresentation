﻿/*************** Customized functions ****************/

function waitMe_show(ele, text) {
	var el = $(ele);
	fontSize = '';
	maxSize = '';
	textPos = 'vertical';
	if (text == null || text == undefined || text == '') {
		text = "Please wait";
	}
	el.waitMe({
		effect: "bounce",
		text: text,
		bg: 'rgba(255,255,255,0.7)',
		color: '#204d74',
		maxSize: maxSize,
		waitTime: -1,
		source: 'img.svg',
		textPos: textPos,
		fontSize: fontSize,
		onClose: function (el) { }
	});
}
function waitMe_hide(ele) {
	var el = $(ele);
	$(el).waitMe('hide');
}
